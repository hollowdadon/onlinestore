module.exports = function(app, passport) {

    var express = require('express');
    var router  = express.Router();
    var Order = require('../app/models/order');
    var Cart = require('../app/models/cart');

    router.use('/', notLoggedIn, (req, res, next) => {
        next();
    });
    
        // HOME PAGE
        app.get('/user', (req, res) => {
            res.render('user/index.ejs'); // load the index.ejs file
        });
    
        // LOGIN
        app.get('/user/login', notLoggedIn, (req, res) => {
    
            // render the page and pass in any flash data if it exists
            res.render('user/login.ejs', { message: req.flash('loginMessage') }); 
        });
    
        // process the login form
        app.post('/user/login', passport.authenticate('local-login', {
            successRedirect : '/', // redirect to the secure profile section
            failureRedirect : '/user/login', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
        // SIGNUP 
        app.get('/user/signup', notLoggedIn, (req, res) => {
    
            // render the page and pass in any flash data if it exists
            res.render('user/signup.ejs', { message: req.flash('signupMessage') });
        });
    
        // process the signup form
        app.post('/user/signup', passport.authenticate('local-signup', {
            successRedirect : '/', // redirect to the secure profile section
            failureRedirect : '/user/signup', // redirect back to the signup page if there is an error
            failureFlash : true // allow flash messages
        }));
    
        // PROFILE SECTION 
        app.get('/user/profiler', isLoggedIn, (req, res) => {
            Order.find({user: req.user}, (err, orders) => {
                if (err) {
                    return res.write('Error dude');
                }
                var cart;
                orders.forEach((order) => {
                    cart = new Cart(order.cart);
                    order.items = cart.generateArray();
                });
                res.render('user/profiler.hbs', {
                    user : req.user, // get the user out of session and pass to template
                    orders: orders
                });
            });
        });
    
        // LOGOUT 
        app.get('/user/logout', (req, res) => {
            req.session.destroy( (err) => {
                res.redirect('/user'); //Inside a callback… bulletproof!
              });
        });

            // route middleware to make sure a user is logged in
    function isLoggedIn(req, res, next) {
        
            // if user is authenticated in the session, carry on 
            if (req.isAuthenticated())
                return next();
        
            // if they aren't redirect them to the home page
            res.redirect('/user');
        }
        
        function notLoggedIn(req, res, next) {
            
                // if user is authenticated in the session, carry on 
                if (!req.isAuthenticated())
                    return next();
            
                // if they aren't redirect them to the home page
                res.redirect('/user/profile');
            }
        
        
    };
    
