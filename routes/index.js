const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const passport = require('passport');

const Products = require('../app/models/product');

const productRouter = express.Router();
const Cart = require('../app/models/cart');
const stripe = require('stripe')('sk_test_xsQxgKw2tUpkwkTLqKQqbv0Y');
const Order = require('../app/models/order');

productRouter.use(bodyParser.json());

productRouter.route('/')
.get((req,res,next) => {
   Products.find({})
   .then((products) => {
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/html');
    res.render('shop/index', { title: 'Shopping Cart', products: products });
   }, (err) => next(err))
    .catch((err) => next(err));
})
.post((req,res,next) => {
    Products.create(req.body)
    .then((product) => {
        console.log('Product created', product);
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(product);
    }, (err) => next(err))
        .catch((err) => next(err));
})
.put((req,res,next) => {
    res.statusCode = 403;
    res.end('Put operation not supporting on products');
})
.delete((req,res,next) => {
    Products.remove({})
    .then((resp) => {
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.json(resp);
    }, (err) => next(err))
        .catch((err) => next(err)); 
});

productRouter.get('/add-to-cart/:id',(req, res, next) => {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart: {items: {}});

    Products.findById(productId, (err, product) => {
        if (err) {
            return res.redirect('/');
            console.log('Sorry but you made an error here');
        }
         cart.add(product, product.id);
         req.session.cart = cart;
         console.log(req.session.cart);
         res.redirect('/');
    });
});

productRouter.get('/delete/:id', (req, res, next) => {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart: {items: {}});

    cart.deleteOneProductItem(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart')
});

productRouter.get('/remove/:id', (req, res, next) => {
    var productId = req.params.id;
    var cart = new Cart(req.session.cart ? req.session.cart: {items: {}});

    cart.deleteOneProductAllItems(productId);
    req.session.cart = cart;
    res.redirect('/shopping-cart')
});

productRouter.get('/shopping-cart', (req, res, next) => {
    if (!req.session.cart) {
        return res.render('shop/shopping-cart', {products: null});
    }

    var cart = new Cart(req.session.cart);
    res.render('shop/shopping-cart', {products: cart.generateArray(), totalPrice: cart.totalPrice,
     totalPriceByOneHandred: cart.totalPrice * 100});
});

productRouter.get('/pay', (req, res, next) => {
    if (!req.session.cart) {
        return res.redirect('shop/shopping-cart');
    }
    var cart = new Cart(req.session.cart);
    res.render('shop/pay.hbs', {total: cart.totalPrice});
});

  
  productRouter.get('/success', (req, res, next) => {
    var payerId = req.query.PayerID;
    var paymentId = req.query.paymentId;
    var cart = new Cart(req.session.cart);
  
    var execute_payment_json = {
      "payer_id": payerId,
      "transactions": [{
          "amount": {
              "currency": "USD",
              "total": cart.totalPrice
          }
      }]
  };
  
  paypal.payment.execute(paymentId, execute_payment_json,  (error, payment) => {
    if (error) {
        console.log(error.response);
        throw error;
    } else {  
        console.log(JSON.stringify(payment));
        res.send('Success');
    }
  });
  });
  
  productRouter.get('/cancel', (req, res, next) => res.send('Cancel payment'));

  productRouter.post('/charge', (req, res, next) => {
    var cart = new Cart(req.session.cart);
    var amount = cart.totalPrice * 100;

    stripe.customers.create({
        email: req.body.stripeEmail,
        source: req.body.stripeToken
    })
    .then(customer => stripe.charges.create({
        amount,
        description:'Fantasy books from Alex store',
        currency:'usd',
        customer:customer.id
    }))
    .then((charge) => {
        res.render('shop/success');
        var order = new Order({
            user: req.user,
            cart: cart,
            paymentId: charge.id
        });
        order.save((err, result) => {
            if(err) {
                res.send('Error here, dude');
            }
        });
        req.session.cart = null;
    });
  });
            
module.exports = productRouter;