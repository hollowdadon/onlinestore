const express  = require('express');
const app      = express();
const port     = process.env.PORT || 3000;
const mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const passport = require('passport');
const flash    = require('connect-flash');
const expressHbs = require('express-handlebars');
const validator = require('express-validator');

const morgan       = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser   = require('body-parser');
const session      = require('express-session');
const MongoStore = require('connect-mongo')(session);


const configDB = require('./config/database.js');

const productRouter = require('./routes/index');

const connect = mongoose.connect(configDB.url, {
  useMongoClient: true
});

connect.then((db) => {
  console.log('Connected correctly to server');
}, (err) => { console.log(err); });


require('./config/passport')(passport); // pass passport for configuration

app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)
app.use(bodyParser()); // get information from html forms
app.use(validator());

app.set('view engine', 'ejs'); // set up ejs for templating
app.engine('.hbs', expressHbs({defaultLayout: 'layout', extname: '.hbs'}));
app.set('view engine', '.hbs');
app.use(express.static(__dirname + '/public'));

// required for passport
app.use(session({ 
    secret: 'ilovescotchscotchyscotchscotch',
    resave: false,
    saveUninitialized: false,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 180 * 60 * 1000 }
})); // session secret
app.use(flash()); // use connect-flash for flash messages stored in session
app.use(passport.initialize());
app.use(passport.session());


app.use((req, res, next) => {
    res.locals.login = req.isAuthenticated();
    res.locals.session = req.session;
    next();
});


// routes
app.use('/', productRouter);
require('./routes/user')(app, passport);

app.listen(port);
console.log('The magic happens on port ' + port);
